﻿using System.Collections.Generic;

namespace Devmil.Querubim.Dominio
{
    public class Perfil
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public int NivelAcesso { get; set; }
        public bool Ativo { get; set; }

        public ICollection<Usuario> Usuarios { get; set; }
    }
}
