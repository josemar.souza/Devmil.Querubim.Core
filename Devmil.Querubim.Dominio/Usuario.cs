﻿namespace Devmil.Querubim.Dominio
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool Ativo { get; set; }
        public int PerfilId { get; set; }
        public int PessoaId { get; set; }
        public int EmpresaId { get; set; }

        /*** Navigation Properties ***/
        public Perfil Perfil { get; set; }
        public Pessoa Pessoa { get; set; }
        public Empresa Empresa { get; set; }
    }
}
