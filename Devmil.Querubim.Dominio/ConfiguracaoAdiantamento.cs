﻿using System;

namespace Devmil.Querubim.Dominio.Modelo
{
    public class ConfiguracaoAdiantamento
    {
        public int Id { get; set; }
        public int TipoAdiantamentoId { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public decimal Valor { get; set; }
        public decimal TaxaAdministrativa { get; set; }
        public int EmpresaId { get; set; }


        /*** Navigation Properties ***/
        public TipoAdiantamento TipoAdiantamento { get; set; }
        public Empresa Empresa { get; set; }
    }
}
