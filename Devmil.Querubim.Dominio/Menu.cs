﻿using System;

namespace Devmil.Querubim.Dominio
{
    public class Menu : IComparable
    {
        public int Id { get; set; }
        public string Nome { get; set; }

        public string Descricao { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Icon { get; set; }
        public int NivelAcesso { get; set; }
        public int? PaiId { get; set; }
        public bool Ativo { get; set; }

        /*** Navigation Properties ***/
        public Menu Pai { get; set; }

        public int CompareTo(object obj)
        {
            Menu other = (Menu)obj;
            if(this.Id > 0 && other.Id > 0)
                return this.Id > other.Id ? 1 : -1;
            else 
                return 0;
        }

        public string getHref(){
            return this.Controller + "/" + this.Action;
        }
    }
}
