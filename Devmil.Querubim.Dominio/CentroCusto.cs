﻿namespace Devmil.Querubim.Dominio
{
    public class CentroCusto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public bool Ativo { get; set; }
        public int EmpresaId { get; set; }

        /*** Navigation Properties ***/
        public Empresa Empresa { get; set; }
    }
}
