﻿namespace Devmil.Querubim.Dominio
{
    public class TipoAdiantamento
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public bool Configuravel { get; set; }
        public bool Ativo { get; set; }

    }
}
