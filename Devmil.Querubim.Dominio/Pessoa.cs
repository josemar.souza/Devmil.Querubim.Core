﻿using System;

namespace Devmil.Querubim.Dominio
{
    public class Pessoa
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public DateTime Nascimento { get; set; }
        public string Sexo { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public string FotoUrl { get; set; }
        public string RacaCor { get; set; }
        public string Escolaridade { get; set; }
        public int? EnderecoId { get; set; }

        /*** Navigation Properties ***/ 
        public Endereco Endereco { get; set; }
        public Usuario Usuario { get; set; }
    }
}
