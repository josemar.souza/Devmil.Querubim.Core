﻿using System;

namespace Devmil.Querubim.Dominio
{
    public class Empregado
    {
        public int Id { get; set; }
        public string Matricula { get; set; }
        public string CtpsNumero { get; set; }
        public string CtpsSerie { get; set; }
        public string CtpsUf { get; set; }
        public string Nis { get; set; }
        public DateTime Admissao { get; set; }
        public DateTime Demissao { get; set; }
        public decimal Salario { get; set; }
        public bool Ativo { get; set; }
        public int PessoaId { get; set; }
        public int EmpresaId { get; set; }
        public int? CentroCustoId { get; set; }
        public int? LotacaoId { get; set; }
        public int FuncaoId { get; set; }
        public int? ContratoId { get; set; }

        /*** Navigation Properties ***/
        public CentroCusto CentroCusto { get; set; }
        public Unidade Lotacao { get; set; }
        public Funcao Funcao { get; set; }
        public Contrato Contrato { get; set; }
        public Pessoa Pessoa { get; set; }
        public Empresa Empresa { get; set; }
    }
}
