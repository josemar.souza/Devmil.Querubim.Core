﻿using System.Collections.Generic;

namespace Devmil.Querubim.Dominio
{
    public class Empresa
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cnpj { get; set; }
        public string LogoUrl { get; set; }
        public bool Ativo { get; set; }
        public int EnderecoId { get; set; }
        public int ResponsavelId { get; set; }

        /*** Navigation Properties ***/
        public Pessoa Responsavel { get; set; }
        public Endereco Endereco { get; set; }

        public ICollection<Usuario> Usuarios { get; set;}
    }
}
