﻿namespace Devmil.Querubim.Dominio
{
    public class Unidade
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public int? ResponsavelId { get; set; }
        public bool Ativo { get; set; }
        public int EmpresaId { get; set; }

        /*** Navigation Properties ***/
        public Pessoa Responsavel { get; set; }
        public Empresa Empresa { get; set; }
    }
}
