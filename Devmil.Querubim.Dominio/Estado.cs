﻿using System.Collections.Generic;

namespace Devmil.Querubim.Dominio
{
    public class Estado
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Uf { get; set; }

        public ICollection<Cidade> Cidades { get; set; }
    }
}
