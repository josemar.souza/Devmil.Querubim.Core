﻿namespace Devmil.Querubim.Dominio
{
    public class Parametro
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Valor { get; set; }
        public bool Ativo { get; set; }
    }
}
