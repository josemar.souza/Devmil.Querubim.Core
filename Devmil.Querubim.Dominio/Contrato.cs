﻿using System;

namespace Devmil.Querubim.Dominio
{
    public class Contrato
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        public string Objeto { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public int ContratanteId { get; set; }
        public int? ResponsavelId { get; set; }
        public decimal Valor { get; set; }
        public int EmpresaId { get; set; }
        
        
        /*** Navigation Properties ***/
        public Empresa Contratante { get; set; }
        public Pessoa Responsavel { get; set; }
        public Empresa Empresa { get; set; }
    }
}
