﻿namespace Devmil.Querubim.Dominio
{
    public class Funcao
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Cbo { get; set; }
        public bool Ativo { get; set; }
        public int EmpresaId { get; set; }

        /*** Navigation Properties ***/
        public Empresa Empresa { get; set; }
    }
}
