﻿using System.Collections.Generic;

namespace Devmil.Querubim.Dominio
{
    public class Cidade
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int EstadoId { get; set; }

        /** Navigation Properties**/
        public Estado Estado { get; set; }        
        public ICollection<Endereco> Enderecos { get; set; }
    }
}
