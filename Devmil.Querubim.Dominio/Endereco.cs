﻿namespace Devmil.Querubim.Dominio
{
    public class Endereco
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public string Cep { get; set; }
        public string TipoLogradouro { get; set; }
        public string Logradouro { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public int CidadeId { get; set; }

        /** Navigation Properties */
        public Cidade Cidade { get; set; }
    }
}
