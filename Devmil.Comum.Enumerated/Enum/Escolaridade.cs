﻿namespace Devmil.Comum.Enumerated
{
    public enum Escolaridade
    {
        Ensino_Fundamental,
        Ensino_Médio,
        Ensino_Superior,
        Pós_Graduação        
    }
}
