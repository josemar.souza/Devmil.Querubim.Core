﻿namespace Devmil.Comum.Enumerated
{
    public enum RecaCor
    {
        Amarela,
        Branca,
        Indígena,
        Parda,
        Preta
    }
}
