﻿namespace Devmil.Comum.Enumerated
{
    public enum TipoLogradouro
    {
        Alameda,
        Avenida,
        Beco,
        Conjunto,
        Ramal,
        Rua,
        Travessa
    }
}
