﻿using System;
using System.Collections.Generic;
using Devmil.Querubim.AcessoDados;
using Devmil.Querubim.Dominio;
using Devmil.Querubim.Repositorio;

namespace Devmil.Querubim.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            MenuRepositorio repo = new MenuRepositorio(new QuerubimContext());
            List<Menu> menus = repo.ToList();
            foreach (var item in menus)
            {
                System.Console.WriteLine(item.Nome);
            }
            System.Console.ReadKey();
        }
    }
}
