using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Devmil.Querubim.AcessoDados;
using Devmil.Querubim.Dominio;

namespace Devmil.Querubim.Repositorio
    {
    public class MenuRepositorio : RepositorioGenerico<Menu>
    {
        public MenuRepositorio(QuerubimContext context) : base(context)
        {
            
        }

        public List<Menu> ToList(){
            return this.GetAll().ToList();
        }

        public Menu FindByName(string nome){
            Expression<Func<Menu,bool>> finder = p => p.Nome == nome;
            return this.FindBy(finder).FirstOrDefault();
        }

        public Menu FindById(int id){
            Expression<Func<Menu,bool>> finder = p => p.Id == id;
            return this.FindBy(finder).FirstOrDefault();
        }

        public List<Menu> FindByPaiId(int paiId){
            Expression<Func<Menu,bool>> finder = p => p.PaiId == paiId;
            return this.FindBy(finder).ToList();
        }
    }
}