using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Devmil.Querubim.AcessoDados;
using Devmil.Querubim.Dominio;
using Microsoft.EntityFrameworkCore;

namespace Devmil.Querubim.Repositorio
{
    public class PessoaRepositorio : RepositorioGenerico<Pessoa>
    {
        public PessoaRepositorio(QuerubimContext context) : base (context)
        {
            
        }
        public List<Pessoa> ToList()
        {
            return this.GetAll().ToList();
        }

        public Pessoa FindById(int id){
            Expression<Func<Pessoa,bool>> finder = p => p.Id == id;
            return this.FindBy(finder).FirstOrDefault();
        }
    }
}