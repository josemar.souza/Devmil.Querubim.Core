using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace Devmil.Querubim.Repositorio
{
    public class RepositorioGenerico<TEntidade> : IGenericRepository<TEntidade>
    where TEntidade : class
    {
        protected DbContext context;

        public RepositorioGenerico(DbContext context){
            this.context = context;
        }
        public void Add(TEntidade entity)
        {
            context.Set<TEntidade>().Add(entity);
        }

        public void Delete(TEntidade entity)
        {
            context.Set<TEntidade>().Remove(entity);
        }

        public void Edit(TEntidade entity)
        {
            context.Entry(entity).State = EntityState.Modified;
        }

        public IQueryable<TEntidade> FindBy(Expression<Func<TEntidade, bool>> predicate)
        {
            IQueryable<TEntidade> query = context.Set<TEntidade>().Where(predicate);
            return query;
        }

        public IQueryable<TEntidade> GetAll()
        {
            IQueryable<TEntidade> query = context.Set<TEntidade>();
            return query;
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}