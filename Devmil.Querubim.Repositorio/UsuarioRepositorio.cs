using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Devmil.Querubim.AcessoDados;
using Devmil.Querubim.Dominio;
using Microsoft.EntityFrameworkCore;

namespace Devmil.Querubim.Repositorio
{
    public class UsuarioRepositorio : RepositorioGenerico<Usuario>
    {
        public UsuarioRepositorio(QuerubimContext context) : base (context)
        {
            
        }
        public List<Usuario> ToList()
        {
            return this.GetAll().ToList();
        }

        public Usuario FindById(int id){
            Expression<Func<Usuario,bool>> finder = p => p.Id == id;
            return this.FindBy(finder).FirstOrDefault();
        }
    }
}