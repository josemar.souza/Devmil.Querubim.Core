using Devmil.Querubim.AcessoDados;
using Devmil.Querubim.Dominio;
using Devmil.Querubim.Repositorio;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace Devmil.Querubim.Web.Controllers
{
    public class CadastrosController : Controller
    {
        private QuerubimContext contexto = new QuerubimContext();
        
        public IActionResult Index()
        {
            MenuRepositorio repoMenu = new MenuRepositorio(contexto);
            Menu menuSelecionado = repoMenu.FindByName("Cadastros");
            ViewBag.menuSelecionado = menuSelecionado;

            ViewBag.TitlePage = menuSelecionado.Nome;
            ViewBag.DescriptionPage = menuSelecionado.Descricao;
            ViewBag.BackPage = "/Home";
            ViewBag.IconPage = menuSelecionado.Icon;
            ViewBag.CustomClass = "";
            ViewBag.TypeView = "";
            
            return View();
        }

        public IActionResult CentroCusto(){
            ViewBag.TitlePage = "Centro de Custo";
            ViewBag.DescriptionPage = "Cadastro de centros de custo";
            ViewBag.BackPage = "/Cadastros";
            ViewBag.IconPage = "extension";
            ViewBag.CustomClass = "";

            return View();
        }

        public IActionResult Pessoa(){
            ViewBag.TitlePage = "Pessoa";
            ViewBag.DescriptionPage = "Cadastro de pessoas";
            ViewBag.BackPage = "/Cadastros";
            ViewBag.NewItem = "/Cadastros/PessoaDetails?new=true";
            ViewBag.IconPage = "people";
            ViewBag.CustomClass = "";

            PessoaRepositorio pessoaRepo = new PessoaRepositorio(contexto);
            ViewBag.Lista = pessoaRepo.ToList();

            return View();
        }
        public IActionResult PessoaDetails(int pessoaId, bool newItem){
            ViewBag.TitlePage = "Pessoa";
            ViewBag.DescriptionPage = "Cadastro de pessoas";
            ViewBag.BackPage = "/Cadastros/Pessoa";
            ViewBag.NewItem = "/Cadastros/PessoaDetails?pessoaId=&newItem=true";
            ViewBag.IconPage = "people";
            ViewBag.CustomClass = "";
            
            Pessoa model = new Pessoa();

            if(!newItem){
                PessoaRepositorio repoPessoa = new PessoaRepositorio(contexto);
                model = repoPessoa.FindById(pessoaId);
            }
            return View(model);
        }
    }
}