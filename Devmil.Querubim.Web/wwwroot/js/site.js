﻿// Write your Javascript code.
$(function() {
    $("#android-content").on("scroll", function() {
        var logo = document.getElementById("android-logo-mobile");
        if($("#android-content").scrollTop() > 50) {
            $("#android-header").addClass("mdl-color--light-green-500");
            logo.src = "/images/querubim_logo_white.png";
        } else {
            $("#android-header").removeClass("mdl-color--light-green-500");
            logo.src = "/images/querubim_logo.png";
        }
    });
});