﻿using System.Collections.Generic;
using Devmil.Querubim.AcessoDados;
using Devmil.Querubim.Repositorio;
using Devmil.Querubim.Dominio;
using Microsoft.AspNetCore.Http;

namespace Devmil.Querubim.Web.Models
{
    public class SistemaViewModel
    {

        public Usuario getUsuario(){
            UsuarioRepositorio repo = new UsuarioRepositorio(new QuerubimContext());
            Usuario user = repo.FindById(1);
            
            return user;
        }

        public List<Menu> getMenu(){           
            MenuRepositorio menuRepositorio = new MenuRepositorio(new QuerubimContext());
            List<Menu> menus = menuRepositorio.ToList();
            menus.Sort();     

            return menus;
        }
        public List<Menu> getMenu(int paiId){           
            MenuRepositorio menuRepositorio = new MenuRepositorio(new QuerubimContext());
            List<Menu> menus = menuRepositorio.FindByPaiId(paiId);
            menus.Sort();     

            return menus;
        }
    }
}