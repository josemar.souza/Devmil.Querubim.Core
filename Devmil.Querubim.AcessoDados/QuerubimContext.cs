using Devmil.Querubim.Dominio;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Devmil.Querubim.AcessoDados
{
    public class QuerubimContext : DbContext
    {
        public DbSet<Estado> Estados { get; set; }
        public DbSet<Cidade> Cidades { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Menu> Menus { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {            
            optionsBuilder.UseNpgsql("Host=localhost;Database=devmil;Username=postgres;Password=postgres");
            //optionsBuilder.UseNpgsql("Host=ec2-184-72-248-8.compute-1.amazonaws.com;Database=dc4jns1ivi7nmn;Username=fenzwgvfghfqhp;Password=bf79d6aa22962d6a7189cfa7835253d5f0f7b1ec26679940d83b6e3b02443499;SslMode=Require");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("querubim");

            //Configura entidade Estado
            modelBuilder.Entity<Estado>(t =>{
                t.ToTable("estado");
                t.HasKey(c=>c.Id).HasName("estado_pk");
                t.Property(p => p.Id).HasColumnName("id");
                t.Property(p => p.Nome).HasColumnName("nome").HasMaxLength(20).IsRequired();
                t.Property(p => p.Uf).HasColumnName("uf").HasMaxLength(2).IsRequired();
            });

            //Configura entidade Cidade
            modelBuilder.Entity<Cidade>(t => {
                t.ToTable("cidade");
                t.HasKey(p=>p.Id).HasName("cidade_pk");
                t.Property(p=>p.Id).HasColumnName("id");
                t.Property(p=>p.Nome).HasColumnName("nome").HasMaxLength(40).IsRequired();
                t.Property(p=>p.EstadoId).HasColumnName("estado_id").IsRequired();
                t.HasOne(p=>p.Estado).WithMany(e=>e.Cidades).HasConstraintName("fk_cidade_estado").IsRequired();
            });

            //Configura entidade Endereco
            modelBuilder.Entity<Endereco>(t => {
                t.ToTable("endereco");
                t.HasKey(p=>p.Id).HasName("endereco_pk");
                t.Property(p=>p.Id).HasColumnName("id");
                t.Property(p=>p.Bairro).HasColumnName("bairro").HasMaxLength(40).IsRequired();
                t.Property(p=>p.Cep).HasColumnName("cep").HasMaxLength(8).IsRequired();
                t.Property(p=>p.CidadeId).HasColumnName("cidade_id").IsRequired();
                t.Property(p=>p.Complemento).HasColumnName("complemento").HasMaxLength(40);
                t.Property(p=>p.Descricao).HasColumnName("descricao").HasMaxLength(20).HasDefaultValue("Principal");
                t.Property(p=>p.Logradouro).HasColumnName("logradouro").HasMaxLength(40).IsRequired();
                t.Property(p=>p.Numero).HasColumnName("numero").HasMaxLength(10);
                t.Property(p=>p.TipoLogradouro).HasColumnName("tipo_logradouro").HasMaxLength(10).IsRequired();
                t.HasOne(e=>e.Cidade).WithMany(c=>c.Enderecos).HasConstraintName("fk_endereco_cidade");
            });

            //Configura entidade Pessoa
            modelBuilder.Entity<Pessoa>(t => {
                t.ToTable("pessoa");
                t.HasKey(p=>p.Id).HasName("pessoa_pk");
                t.Property(p=>p.Id).HasColumnName("id");
                t.Property(p=>p.Cpf).HasColumnName("cpf").HasMaxLength(11).IsRequired();
                t.Property(p => p.Nome).HasColumnName("nome").HasMaxLength(40).IsRequired();

                /*HasColumnType("date") poder não ser compatível com bancos que não seja PostgreSql */
                t.Property(p=>p.Nascimento).HasColumnName("nascimento").HasColumnType("date");
                
                t.Property(p=>p.RacaCor).HasColumnName("raca_cor").HasMaxLength(10);
                t.Property(p=>p.Sexo).HasColumnName("sexo").HasMaxLength(10);
                t.Property(p=>p.Email).HasColumnName("email").HasMaxLength(40);
                t.Property(p=>p.Telefone).HasColumnName("telefone").HasMaxLength(11);
                t.Property(p=>p.Escolaridade).HasColumnName("escolaridade").HasMaxLength(20);
                t.Property(p=>p.FotoUrl).HasColumnName("foto_url").HasMaxLength(40);
                t.Property(p=>p.EnderecoId).HasColumnName("endereco_id");
                t.HasOne(e => e.Endereco);

                t.HasIndex(p=>p.Cpf).IsUnique().HasName("ix_unique_pessoa_cpf");
                t.HasIndex(p=>p.Email).IsUnique().HasName("ix_unique_pessoa_email");
            });

            //Configura entidade Empresa
            modelBuilder.Entity<Empresa>(t => {
                t.ToTable("empresa");
                t.HasKey(p => p.Id).HasName("empresa_pk");
                t.Property(p => p.Id).HasColumnName("id");
                t.Property(p => p.Cnpj).HasColumnName("cnpj").HasMaxLength(14).IsRequired();
                t.Property(p => p.Nome).HasColumnName("nome").HasMaxLength(80).IsRequired();
                t.Property(p => p.LogoUrl).HasColumnName("logo_url").HasMaxLength(40);
                t.Property(p=>p.Ativo).HasColumnName("ativo").HasDefaultValueSql("true");
                t.Property(p=>p.ResponsavelId).HasColumnName("responsavel_id").IsRequired();
                t.Property(p=>p.EnderecoId).HasColumnName("endereco_id");

                t.HasOne(e=>e.Endereco);
                t.HasOne(e => e.Responsavel);

                t.HasIndex(p=>p.Cnpj).IsUnique().HasName("ix_unique_empresa_cnpj");
            });        
            
            //Configura entidade Menu
            modelBuilder.Entity<Menu>(t => {
                t.ToTable("menu");
                t.HasKey(p => p.Id).HasName("menu_pk");
                t.Property(p=>p.Id).HasColumnName("id");
                t.Property(p=>p.Nome).HasColumnName("nome").HasMaxLength(20).IsRequired();
                t.Property(p=>p.Descricao).HasColumnName("descricao").HasMaxLength(80).IsRequired();
                t.Property(p => p.Action).HasColumnName("action").HasMaxLength(20);
                t.Property(p=>p.Controller).HasColumnName("controller").HasMaxLength(20);
                t.Property(p=>p.NivelAcesso).HasColumnName("nivel_acesso");
                t.Property(p=>p.Icon).HasColumnName("icon").HasMaxLength(20);
                t.Property(p=>p.PaiId).HasColumnName("pai_id");
                t.Property(p=>p.Ativo).HasColumnName("ativo").HasDefaultValueSql("true");

                t.HasOne(e => e.Pai);
            });

            //Configura entidade Perfil
            modelBuilder.Entity<Perfil>(t => {
                t.ToTable("perfil");
                t.HasKey(p=>p.Id).HasName("perfil_pk");
                t.Property(p=>p.Id).HasColumnName("id");
                t.Property(p=>p.Nome).HasColumnName("nome").HasMaxLength(20).IsRequired();
                t.Property(p=>p.Descricao).HasColumnName("descricao").HasMaxLength(40).IsRequired();
                t.Property(p=>p.NivelAcesso).HasColumnName("nivel_acesso").IsRequired();
                t.Property(p=>p.Ativo).HasColumnName("ativo").HasDefaultValueSql("true");
            });

            //Configura entidade Usuario
            modelBuilder.Entity<Usuario>(t => {
                t.ToTable("usuario");
                t.HasKey(p=>p.Id).HasName("usuario_pk");
                t.Property(p=>p.Id).HasColumnName("id");
                t.Property(p=>p.Username).HasColumnName("username").HasMaxLength(20).IsRequired();
                t.Property(p=>p.Password).HasColumnName("password").HasMaxLength(40).IsRequired();
                t.Property(p=>p.Ativo).HasColumnName("ativo").HasDefaultValueSql("true");
                t.Property(p=>p.PessoaId).HasColumnName("pessoa_id");
                t.Property(p=>p.PerfilId).HasColumnName("perfil_id");
                t.Property(p=>p.EmpresaId).HasColumnName("empresa_id");

                t.HasOne(e=>e.Pessoa).WithOne(p=>p.Usuario).IsRequired().HasConstraintName("fk_usuario_pessoa");
                t.HasOne(e=>e.Perfil).WithMany(p=>p.Usuarios).IsRequired().HasConstraintName("fk_usuario_perfil");;
                t.HasOne(e=>e.Empresa).WithMany(p=>p.Usuarios).IsRequired().HasConstraintName("fk_usuario_empresa");

                t.HasIndex(p=>p.Username).IsUnique().HasName("ix_unique_usuario_username");
                t.HasIndex(p=>p.PessoaId).IsUnique().HasName("ix_unique_usuario_pessoa_id");
            });
        }
    }
}