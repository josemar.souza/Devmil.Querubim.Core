﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Devmil.Querubim.AcessoDados.Entity.Migrations
{
    public partial class step7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_usuario_emprsa",
                schema: "querubim",
                table: "usuario");

            migrationBuilder.AddColumn<string>(
                name: "descricao",
                schema: "querubim",
                table: "menu",
                maxLength: 80,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddForeignKey(
                name: "fk_usuario_empresa",
                schema: "querubim",
                table: "usuario",
                column: "empresa_id",
                principalSchema: "querubim",
                principalTable: "empresa",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_usuario_empresa",
                schema: "querubim",
                table: "usuario");

            migrationBuilder.DropColumn(
                name: "descricao",
                schema: "querubim",
                table: "menu");

            migrationBuilder.AddForeignKey(
                name: "fk_usuario_emprsa",
                schema: "querubim",
                table: "usuario",
                column: "empresa_id",
                principalSchema: "querubim",
                principalTable: "empresa",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
