﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Devmil.Querubim.AcessoDados.Entity.Migrations
{
    public partial class step4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                schema: "querubim",
                table: "menu",
                newName: "id");

            migrationBuilder.RenameColumn(
                name: "Id",
                schema: "querubim",
                table: "empresa",
                newName: "id");

            migrationBuilder.AlterColumn<bool>(
                name: "ativo",
                schema: "querubim",
                table: "menu",
                nullable: false,
                defaultValueSql: "true",
                oldClrType: typeof(bool));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "id",
                schema: "querubim",
                table: "menu",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "id",
                schema: "querubim",
                table: "empresa",
                newName: "Id");

            migrationBuilder.AlterColumn<bool>(
                name: "ativo",
                schema: "querubim",
                table: "menu",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "true");
        }
    }
}
