﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Devmil.Querubim.AcessoDados.Entity.Migrations
{
    public partial class step1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Estados",
                table: "Estados");

            migrationBuilder.EnsureSchema(
                name: "querubim");

            migrationBuilder.RenameTable(
                name: "Estados",
                newName: "estado",
                newSchema: "querubim");

            migrationBuilder.RenameColumn(
                name: "Uf",
                schema: "querubim",
                table: "estado",
                newName: "uf");

            migrationBuilder.RenameColumn(
                name: "Nome",
                schema: "querubim",
                table: "estado",
                newName: "nome");

            migrationBuilder.RenameColumn(
                name: "Id",
                schema: "querubim",
                table: "estado",
                newName: "id");

            migrationBuilder.AlterColumn<string>(
                name: "uf",
                schema: "querubim",
                table: "estado",
                maxLength: 2,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "nome",
                schema: "querubim",
                table: "estado",
                maxLength: 20,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "estado_pk",
                schema: "querubim",
                table: "estado",
                column: "id");

            migrationBuilder.CreateTable(
                name: "cidade",
                schema: "querubim",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    estado_id = table.Column<int>(nullable: false),
                    nome = table.Column<string>(maxLength: 40, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("cidade_pk", x => x.Id);
                    table.ForeignKey(
                        name: "FK_cidade_estado_estado_id",
                        column: x => x.estado_id,
                        principalSchema: "querubim",
                        principalTable: "estado",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_cidade_estado_id",
                schema: "querubim",
                table: "cidade",
                column: "estado_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "cidade",
                schema: "querubim");

            migrationBuilder.DropPrimaryKey(
                name: "estado_pk",
                schema: "querubim",
                table: "estado");

            migrationBuilder.RenameTable(
                name: "estado",
                schema: "querubim",
                newName: "Estados");

            migrationBuilder.RenameColumn(
                name: "uf",
                table: "Estados",
                newName: "Uf");

            migrationBuilder.RenameColumn(
                name: "nome",
                table: "Estados",
                newName: "Nome");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "Estados",
                newName: "Id");

            migrationBuilder.AlterColumn<string>(
                name: "Uf",
                table: "Estados",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 2);

            migrationBuilder.AlterColumn<string>(
                name: "Nome",
                table: "Estados",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 20);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Estados",
                table: "Estados",
                column: "Id");
        }
    }
}
