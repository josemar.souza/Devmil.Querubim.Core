﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Devmil.Querubim.AcessoDados.Entity.Migrations
{
    public partial class step2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_cidade_estado_estado_id",
                schema: "querubim",
                table: "cidade");

            migrationBuilder.RenameColumn(
                name: "Id",
                schema: "querubim",
                table: "cidade",
                newName: "id");

            migrationBuilder.CreateTable(
                name: "endereco",
                schema: "querubim",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    bairro = table.Column<string>(maxLength: 40, nullable: false),
                    cep = table.Column<string>(maxLength: 8, nullable: false),
                    cidade_id = table.Column<int>(nullable: false),
                    complemento = table.Column<string>(maxLength: 40, nullable: true),
                    descricao = table.Column<string>(maxLength: 20, nullable: true, defaultValue: "Principal"),
                    logradouro = table.Column<string>(maxLength: 40, nullable: false),
                    numero = table.Column<string>(maxLength: 10, nullable: true),
                    tipo_logradouro = table.Column<string>(maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("endereco_pk", x => x.id);
                    table.ForeignKey(
                        name: "fk_endereco_cidade",
                        column: x => x.cidade_id,
                        principalSchema: "querubim",
                        principalTable: "cidade",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_endereco_cidade_id",
                schema: "querubim",
                table: "endereco",
                column: "cidade_id");

            migrationBuilder.AddForeignKey(
                name: "fk_cidade_estado",
                schema: "querubim",
                table: "cidade",
                column: "estado_id",
                principalSchema: "querubim",
                principalTable: "estado",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_cidade_estado",
                schema: "querubim",
                table: "cidade");

            migrationBuilder.DropTable(
                name: "endereco",
                schema: "querubim");

            migrationBuilder.RenameColumn(
                name: "id",
                schema: "querubim",
                table: "cidade",
                newName: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_cidade_estado_estado_id",
                schema: "querubim",
                table: "cidade",
                column: "estado_id",
                principalSchema: "querubim",
                principalTable: "estado",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
