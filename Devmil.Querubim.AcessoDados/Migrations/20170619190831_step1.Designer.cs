﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Devmil.Querubim.AcessoDados.Entity;

namespace Devmil.Querubim.AcessoDados.Entity.Migrations
{
    [DbContext(typeof(QuerubimContext))]
    [Migration("20170619190831_step1")]
    partial class step1
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasDefaultSchema("querubim")
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "1.1.2");

            modelBuilder.Entity("Devmil.Querubim.Dominio.Cidade", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("EstadoId")
                        .HasColumnName("estado_id");

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnName("nome")
                        .HasMaxLength(40);

                    b.HasKey("Id")
                        .HasName("cidade_pk");

                    b.HasIndex("EstadoId");

                    b.ToTable("cidade");
                });

            modelBuilder.Entity("Devmil.Querubim.Dominio.Estado", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnName("nome")
                        .HasMaxLength(20);

                    b.Property<string>("Uf")
                        .IsRequired()
                        .HasColumnName("uf")
                        .HasMaxLength(2);

                    b.HasKey("Id")
                        .HasName("estado_pk");

                    b.ToTable("estado");
                });

            modelBuilder.Entity("Devmil.Querubim.Dominio.Cidade", b =>
                {
                    b.HasOne("Devmil.Querubim.Dominio.Estado", "Estado")
                        .WithMany("Cidades")
                        .HasForeignKey("EstadoId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
