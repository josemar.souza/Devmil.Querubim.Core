﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Devmil.Querubim.AcessoDados.Entity.Migrations
{
    public partial class step6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_usuario_empresa_empresa_id",
                schema: "querubim",
                table: "usuario");

            migrationBuilder.DropForeignKey(
                name: "FK_usuario_perfil_perfil_id",
                schema: "querubim",
                table: "usuario");

            migrationBuilder.DropForeignKey(
                name: "FK_usuario_pessoa_pessoa_id",
                schema: "querubim",
                table: "usuario");

            migrationBuilder.RenameIndex(
                name: "IX_usuario_pessoa_id",
                schema: "querubim",
                table: "usuario",
                newName: "ix_unique_usuario_pessoa_id");

            migrationBuilder.CreateIndex(
                name: "ix_unique_usuario_username",
                schema: "querubim",
                table: "usuario",
                column: "username",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "fk_usuario_emprsa",
                schema: "querubim",
                table: "usuario",
                column: "empresa_id",
                principalSchema: "querubim",
                principalTable: "empresa",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_usuario_perfil",
                schema: "querubim",
                table: "usuario",
                column: "perfil_id",
                principalSchema: "querubim",
                principalTable: "perfil",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_usuario_pessoa",
                schema: "querubim",
                table: "usuario",
                column: "pessoa_id",
                principalSchema: "querubim",
                principalTable: "pessoa",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_usuario_emprsa",
                schema: "querubim",
                table: "usuario");

            migrationBuilder.DropForeignKey(
                name: "fk_usuario_perfil",
                schema: "querubim",
                table: "usuario");

            migrationBuilder.DropForeignKey(
                name: "fk_usuario_pessoa",
                schema: "querubim",
                table: "usuario");

            migrationBuilder.DropIndex(
                name: "ix_unique_usuario_username",
                schema: "querubim",
                table: "usuario");

            migrationBuilder.RenameIndex(
                name: "ix_unique_usuario_pessoa_id",
                schema: "querubim",
                table: "usuario",
                newName: "IX_usuario_pessoa_id");

            migrationBuilder.AddForeignKey(
                name: "FK_usuario_empresa_empresa_id",
                schema: "querubim",
                table: "usuario",
                column: "empresa_id",
                principalSchema: "querubim",
                principalTable: "empresa",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_usuario_perfil_perfil_id",
                schema: "querubim",
                table: "usuario",
                column: "perfil_id",
                principalSchema: "querubim",
                principalTable: "perfil",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_usuario_pessoa_pessoa_id",
                schema: "querubim",
                table: "usuario",
                column: "pessoa_id",
                principalSchema: "querubim",
                principalTable: "pessoa",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
