﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Devmil.Querubim.AcessoDados.Entity.Migrations
{
    public partial class step3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "menu",
                schema: "querubim",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    action = table.Column<string>(maxLength: 20, nullable: true),
                    ativo = table.Column<bool>(nullable: false),
                    controller = table.Column<string>(maxLength: 20, nullable: true),
                    icon = table.Column<string>(maxLength: 20, nullable: true),
                    nivel_acesso = table.Column<int>(nullable: false),
                    nome = table.Column<string>(maxLength: 20, nullable: true),
                    pai_id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("menu_pk", x => x.Id);
                    table.ForeignKey(
                        name: "FK_menu_menu_pai_id",
                        column: x => x.pai_id,
                        principalSchema: "querubim",
                        principalTable: "menu",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "pessoa",
                schema: "querubim",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Cpf = table.Column<string>(maxLength: 11, nullable: false),
                    email = table.Column<string>(maxLength: 40, nullable: true),
                    endereco_id = table.Column<int>(nullable: true),
                    escolaridade = table.Column<string>(maxLength: 20, nullable: true),
                    foto_url = table.Column<string>(maxLength: 40, nullable: true),
                    nascimento = table.Column<DateTime>(type: "date", nullable: false),
                    nome = table.Column<string>(maxLength: 40, nullable: false),
                    raca_cor = table.Column<string>(maxLength: 10, nullable: true),
                    sexo = table.Column<string>(maxLength: 10, nullable: true),
                    telefone = table.Column<string>(maxLength: 11, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pessoa_pk", x => x.id);
                    table.ForeignKey(
                        name: "FK_pessoa_endereco_endereco_id",
                        column: x => x.endereco_id,
                        principalSchema: "querubim",
                        principalTable: "endereco",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "empresa",
                schema: "querubim",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ativo = table.Column<bool>(nullable: false, defaultValueSql: "true"),
                    cnpj = table.Column<string>(maxLength: 14, nullable: false),
                    endereco_id = table.Column<int>(nullable: false),
                    logo_url = table.Column<string>(maxLength: 40, nullable: true),
                    nome = table.Column<string>(maxLength: 80, nullable: false),
                    responsavel_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("empresa_pk", x => x.Id);
                    table.ForeignKey(
                        name: "FK_empresa_endereco_endereco_id",
                        column: x => x.endereco_id,
                        principalSchema: "querubim",
                        principalTable: "endereco",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_empresa_pessoa_responsavel_id",
                        column: x => x.responsavel_id,
                        principalSchema: "querubim",
                        principalTable: "pessoa",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_unique_empresa_cnpj",
                schema: "querubim",
                table: "empresa",
                column: "cnpj",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_empresa_endereco_id",
                schema: "querubim",
                table: "empresa",
                column: "endereco_id");

            migrationBuilder.CreateIndex(
                name: "IX_empresa_responsavel_id",
                schema: "querubim",
                table: "empresa",
                column: "responsavel_id");

            migrationBuilder.CreateIndex(
                name: "IX_menu_pai_id",
                schema: "querubim",
                table: "menu",
                column: "pai_id");

            migrationBuilder.CreateIndex(
                name: "ix_unique_pessoa_cpf",
                schema: "querubim",
                table: "pessoa",
                column: "Cpf",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_unique_pessoa_email",
                schema: "querubim",
                table: "pessoa",
                column: "email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_pessoa_endereco_id",
                schema: "querubim",
                table: "pessoa",
                column: "endereco_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "empresa",
                schema: "querubim");

            migrationBuilder.DropTable(
                name: "menu",
                schema: "querubim");

            migrationBuilder.DropTable(
                name: "pessoa",
                schema: "querubim");
        }
    }
}
