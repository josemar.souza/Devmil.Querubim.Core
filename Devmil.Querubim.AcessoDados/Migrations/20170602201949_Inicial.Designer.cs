﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Devmil.Querubim.AcessoDados.Entity;

namespace Devmil.Querubim.AcessoDados.Entity.Migrations
{
    [DbContext(typeof(QuerubimContext))]
    [Migration("20170602201949_Inicial")]
    partial class Inicial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "1.1.2");

            modelBuilder.Entity("Devmil.Querubim.Dominio.Estado", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Nome");

                    b.Property<string>("Uf");

                    b.HasKey("Id");

                    b.ToTable("Estados");
                });
        }
    }
}
