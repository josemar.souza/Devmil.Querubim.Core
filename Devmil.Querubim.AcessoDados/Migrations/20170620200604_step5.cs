﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Devmil.Querubim.AcessoDados.Entity.Migrations
{
    public partial class step5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Cpf",
                schema: "querubim",
                table: "pessoa",
                newName: "cpf");

            migrationBuilder.AlterColumn<string>(
                name: "nome",
                schema: "querubim",
                table: "menu",
                maxLength: 20,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 20,
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "perfil",
                schema: "querubim",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ativo = table.Column<bool>(nullable: false, defaultValueSql: "true"),
                    descricao = table.Column<string>(maxLength: 40, nullable: false),
                    nivel_acesso = table.Column<int>(nullable: false),
                    nome = table.Column<string>(maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("perfil_pk", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "usuario",
                schema: "querubim",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ativo = table.Column<bool>(nullable: false, defaultValueSql: "true"),
                    empresa_id = table.Column<int>(nullable: false),
                    password = table.Column<string>(maxLength: 40, nullable: false),
                    perfil_id = table.Column<int>(nullable: false),
                    pessoa_id = table.Column<int>(nullable: false),
                    username = table.Column<string>(maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("usuario_pk", x => x.id);
                    table.ForeignKey(
                        name: "FK_usuario_empresa_empresa_id",
                        column: x => x.empresa_id,
                        principalSchema: "querubim",
                        principalTable: "empresa",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_usuario_perfil_perfil_id",
                        column: x => x.perfil_id,
                        principalSchema: "querubim",
                        principalTable: "perfil",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_usuario_pessoa_pessoa_id",
                        column: x => x.pessoa_id,
                        principalSchema: "querubim",
                        principalTable: "pessoa",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_usuario_empresa_id",
                schema: "querubim",
                table: "usuario",
                column: "empresa_id");

            migrationBuilder.CreateIndex(
                name: "IX_usuario_perfil_id",
                schema: "querubim",
                table: "usuario",
                column: "perfil_id");

            migrationBuilder.CreateIndex(
                name: "IX_usuario_pessoa_id",
                schema: "querubim",
                table: "usuario",
                column: "pessoa_id",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "usuario",
                schema: "querubim");

            migrationBuilder.DropTable(
                name: "perfil",
                schema: "querubim");

            migrationBuilder.RenameColumn(
                name: "cpf",
                schema: "querubim",
                table: "pessoa",
                newName: "Cpf");

            migrationBuilder.AlterColumn<string>(
                name: "nome",
                schema: "querubim",
                table: "menu",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 20);
        }
    }
}
