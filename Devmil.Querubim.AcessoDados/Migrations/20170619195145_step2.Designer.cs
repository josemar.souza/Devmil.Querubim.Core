﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Devmil.Querubim.AcessoDados.Entity;

namespace Devmil.Querubim.AcessoDados.Entity.Migrations
{
    [DbContext(typeof(QuerubimContext))]
    [Migration("20170619195145_step2")]
    partial class step2
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasDefaultSchema("querubim")
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "1.1.2");

            modelBuilder.Entity("Devmil.Querubim.Dominio.Cidade", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<int>("EstadoId")
                        .HasColumnName("estado_id");

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnName("nome")
                        .HasMaxLength(40);

                    b.HasKey("Id")
                        .HasName("cidade_pk");

                    b.HasIndex("EstadoId");

                    b.ToTable("cidade");
                });

            modelBuilder.Entity("Devmil.Querubim.Dominio.Endereco", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<string>("Bairro")
                        .IsRequired()
                        .HasColumnName("bairro")
                        .HasMaxLength(40);

                    b.Property<string>("Cep")
                        .IsRequired()
                        .HasColumnName("cep")
                        .HasMaxLength(8);

                    b.Property<int>("CidadeId")
                        .HasColumnName("cidade_id");

                    b.Property<string>("Complemento")
                        .HasColumnName("complemento")
                        .HasMaxLength(40);

                    b.Property<string>("Descricao")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("descricao")
                        .HasDefaultValue("Principal")
                        .HasMaxLength(20);

                    b.Property<string>("Logradouro")
                        .IsRequired()
                        .HasColumnName("logradouro")
                        .HasMaxLength(40);

                    b.Property<string>("Numero")
                        .HasColumnName("numero")
                        .HasMaxLength(10);

                    b.Property<string>("TipoLogradouro")
                        .IsRequired()
                        .HasColumnName("tipo_logradouro")
                        .HasMaxLength(10);

                    b.HasKey("Id")
                        .HasName("endereco_pk");

                    b.HasIndex("CidadeId");

                    b.ToTable("endereco");
                });

            modelBuilder.Entity("Devmil.Querubim.Dominio.Estado", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnName("nome")
                        .HasMaxLength(20);

                    b.Property<string>("Uf")
                        .IsRequired()
                        .HasColumnName("uf")
                        .HasMaxLength(2);

                    b.HasKey("Id")
                        .HasName("estado_pk");

                    b.ToTable("estado");
                });

            modelBuilder.Entity("Devmil.Querubim.Dominio.Cidade", b =>
                {
                    b.HasOne("Devmil.Querubim.Dominio.Estado", "Estado")
                        .WithMany("Cidades")
                        .HasForeignKey("EstadoId")
                        .HasConstraintName("fk_cidade_estado")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Devmil.Querubim.Dominio.Endereco", b =>
                {
                    b.HasOne("Devmil.Querubim.Dominio.Cidade", "Cidade")
                        .WithMany("Enderecos")
                        .HasForeignKey("CidadeId")
                        .HasConstraintName("fk_endereco_cidade")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
