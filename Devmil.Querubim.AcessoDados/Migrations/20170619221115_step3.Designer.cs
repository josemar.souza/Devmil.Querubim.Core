﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Devmil.Querubim.AcessoDados.Entity;

namespace Devmil.Querubim.AcessoDados.Entity.Migrations
{
    [DbContext(typeof(QuerubimContext))]
    [Migration("20170619221115_step3")]
    partial class step3
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasDefaultSchema("querubim")
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "1.1.2");

            modelBuilder.Entity("Devmil.Querubim.Dominio.Cidade", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<int>("EstadoId")
                        .HasColumnName("estado_id");

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnName("nome")
                        .HasMaxLength(40);

                    b.HasKey("Id")
                        .HasName("cidade_pk");

                    b.HasIndex("EstadoId");

                    b.ToTable("cidade");
                });

            modelBuilder.Entity("Devmil.Querubim.Dominio.Empresa", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Ativo")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ativo")
                        .HasDefaultValueSql("true");

                    b.Property<string>("Cnpj")
                        .IsRequired()
                        .HasColumnName("cnpj")
                        .HasMaxLength(14);

                    b.Property<int>("EnderecoId")
                        .HasColumnName("endereco_id");

                    b.Property<string>("LogoUrl")
                        .HasColumnName("logo_url")
                        .HasMaxLength(40);

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnName("nome")
                        .HasMaxLength(80);

                    b.Property<int>("ResponsavelId")
                        .HasColumnName("responsavel_id");

                    b.HasKey("Id")
                        .HasName("empresa_pk");

                    b.HasIndex("Cnpj")
                        .IsUnique()
                        .HasName("ix_unique_empresa_cnpj");

                    b.HasIndex("EnderecoId");

                    b.HasIndex("ResponsavelId");

                    b.ToTable("empresa");
                });

            modelBuilder.Entity("Devmil.Querubim.Dominio.Endereco", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<string>("Bairro")
                        .IsRequired()
                        .HasColumnName("bairro")
                        .HasMaxLength(40);

                    b.Property<string>("Cep")
                        .IsRequired()
                        .HasColumnName("cep")
                        .HasMaxLength(8);

                    b.Property<int>("CidadeId")
                        .HasColumnName("cidade_id");

                    b.Property<string>("Complemento")
                        .HasColumnName("complemento")
                        .HasMaxLength(40);

                    b.Property<string>("Descricao")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("descricao")
                        .HasDefaultValue("Principal")
                        .HasMaxLength(20);

                    b.Property<string>("Logradouro")
                        .IsRequired()
                        .HasColumnName("logradouro")
                        .HasMaxLength(40);

                    b.Property<string>("Numero")
                        .HasColumnName("numero")
                        .HasMaxLength(10);

                    b.Property<string>("TipoLogradouro")
                        .IsRequired()
                        .HasColumnName("tipo_logradouro")
                        .HasMaxLength(10);

                    b.HasKey("Id")
                        .HasName("endereco_pk");

                    b.HasIndex("CidadeId");

                    b.ToTable("endereco");
                });

            modelBuilder.Entity("Devmil.Querubim.Dominio.Estado", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnName("nome")
                        .HasMaxLength(20);

                    b.Property<string>("Uf")
                        .IsRequired()
                        .HasColumnName("uf")
                        .HasMaxLength(2);

                    b.HasKey("Id")
                        .HasName("estado_pk");

                    b.ToTable("estado");
                });

            modelBuilder.Entity("Devmil.Querubim.Dominio.Menu", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Action")
                        .HasColumnName("action")
                        .HasMaxLength(20);

                    b.Property<bool>("Ativo")
                        .HasColumnName("ativo");

                    b.Property<string>("Controller")
                        .HasColumnName("controller")
                        .HasMaxLength(20);

                    b.Property<string>("Icon")
                        .HasColumnName("icon")
                        .HasMaxLength(20);

                    b.Property<int>("NivelAcesso")
                        .HasColumnName("nivel_acesso");

                    b.Property<string>("Nome")
                        .HasColumnName("nome")
                        .HasMaxLength(20);

                    b.Property<int?>("PaiId")
                        .HasColumnName("pai_id");

                    b.HasKey("Id")
                        .HasName("menu_pk");

                    b.HasIndex("PaiId");

                    b.ToTable("menu");
                });

            modelBuilder.Entity("Devmil.Querubim.Dominio.Pessoa", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<string>("Cpf")
                        .IsRequired()
                        .HasMaxLength(11);

                    b.Property<string>("Email")
                        .HasColumnName("email")
                        .HasMaxLength(40);

                    b.Property<int?>("EnderecoId")
                        .HasColumnName("endereco_id");

                    b.Property<string>("Escolaridade")
                        .HasColumnName("escolaridade")
                        .HasMaxLength(20);

                    b.Property<string>("FotoUrl")
                        .HasColumnName("foto_url")
                        .HasMaxLength(40);

                    b.Property<DateTime>("Nascimento")
                        .HasColumnName("nascimento")
                        .HasColumnType("date");

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnName("nome")
                        .HasMaxLength(40);

                    b.Property<string>("RacaCor")
                        .HasColumnName("raca_cor")
                        .HasMaxLength(10);

                    b.Property<string>("Sexo")
                        .HasColumnName("sexo")
                        .HasMaxLength(10);

                    b.Property<string>("Telefone")
                        .HasColumnName("telefone")
                        .HasMaxLength(11);

                    b.HasKey("Id")
                        .HasName("pessoa_pk");

                    b.HasIndex("Cpf")
                        .IsUnique()
                        .HasName("ix_unique_pessoa_cpf");

                    b.HasIndex("Email")
                        .IsUnique()
                        .HasName("ix_unique_pessoa_email");

                    b.HasIndex("EnderecoId");

                    b.ToTable("pessoa");
                });

            modelBuilder.Entity("Devmil.Querubim.Dominio.Cidade", b =>
                {
                    b.HasOne("Devmil.Querubim.Dominio.Estado", "Estado")
                        .WithMany("Cidades")
                        .HasForeignKey("EstadoId")
                        .HasConstraintName("fk_cidade_estado")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Devmil.Querubim.Dominio.Empresa", b =>
                {
                    b.HasOne("Devmil.Querubim.Dominio.Endereco", "Endereco")
                        .WithMany()
                        .HasForeignKey("EnderecoId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Devmil.Querubim.Dominio.Pessoa", "Responsavel")
                        .WithMany()
                        .HasForeignKey("ResponsavelId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Devmil.Querubim.Dominio.Endereco", b =>
                {
                    b.HasOne("Devmil.Querubim.Dominio.Cidade", "Cidade")
                        .WithMany("Enderecos")
                        .HasForeignKey("CidadeId")
                        .HasConstraintName("fk_endereco_cidade")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Devmil.Querubim.Dominio.Menu", b =>
                {
                    b.HasOne("Devmil.Querubim.Dominio.Menu", "Pai")
                        .WithMany()
                        .HasForeignKey("PaiId");
                });

            modelBuilder.Entity("Devmil.Querubim.Dominio.Pessoa", b =>
                {
                    b.HasOne("Devmil.Querubim.Dominio.Endereco", "Endereco")
                        .WithMany()
                        .HasForeignKey("EnderecoId");
                });
        }
    }
}
